import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TextInput, Button, TouchableHighlight, TouchableOpacity, TouchableOpacityBase } from 'react-native'
import logo from './Assets/Images/logo.png'

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoWrapp}>
          <Image source={logo} style={styles.logo} />
        </View>
        <View style={styles.loginWrapp}>
          <Text style={styles.loginText}>Login</Text>
        </View>
        <View>
          <Text style={{marginBottom: 5, color: '#003366'}}>Username / Email</Text>
          <TextInput style={styles.textInput}/>
        </View>
        <View style={{marginTop: 10}}>
          <Text style={{marginBottom: 5, color: '#003366'}}>Password</Text>
          <TextInput style={styles.textInput}/>
        </View>
        <View style={styles.buttonWrapp}>
          <TouchableOpacity style={styles.touchLogin}>
            <Text style={{color: 'white', fontSize: 18}}>Masuk</Text>
          </TouchableOpacity>
          <View style={{alignItems: 'center', marginVertical: 15}}>
            <Text style={{color: '#3EC6FF', fontSize: 18}}>Atau</Text>
          </View>
          <TouchableOpacity style={styles.touchDaftar}>
            <Text style={{color: 'white', fontSize: 18}}>Daftar ?</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    paddingHorizontal: 20
  },
  logoWrapp: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20
  },
  logo: {
    height: 100,
    resizeMode: 'contain' 
  },
  loginWrapp: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 15
  },
  loginText: {
    fontSize: 24, 
    fontFamily: 'Roboto', 
    color: '#003366'
  }, 
  textInput: {
    borderWidth: 2, 
    borderColor:'#003366', 
    height: 50, 
    paddingHorizontal: 15, 
    fontSize: 16, 
    color: '#003366'
  },
  buttonWrapp: {
    paddingHorizontal: 50,
    marginVertical: 20,
  },
  touchLogin: {
    backgroundColor: '#3EC6FF',
    height: 50, 
    alignItems: 'center', 
    borderRadius: 25, 
    justifyContent: 'center'
  },
  touchDaftar: {
    backgroundColor: '#003366',
    height: 50, 
    alignItems: 'center', 
    borderRadius: 25, 
    justifyContent: 'center'
  }
})
