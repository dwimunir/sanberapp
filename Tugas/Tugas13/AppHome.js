import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import LoginScreen from './LoginScreen'
import AboutScreen from './AboutScreen'

export default class AppHome extends Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <LoginScreen /> */}
        <AboutScreen />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})