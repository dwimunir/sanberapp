import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, ScrollView } from 'react-native'
import { FontAwesome, AntDesign } from '@expo/vector-icons';

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{paddingHorizontal: 10}}>
          <View style={{alignItems: 'center', marginTop: 20}}>
            <Text style={{fontSize: 24, fontWeight: 'bold', color: '#003366'}}>Tentang Saya</Text>
          </View>
          <View style={{alignItems: 'center'}}>
            <FontAwesome name="user-circle" size={200} color="gray" />
          </View>
          <View style={{alignItems: 'center'}}>
            <Text style={{fontSize: 20, color: '#003366', fontWeight: 'bold'}}>Dwi Misbachul Munir</Text>
            <Text style={{color: '#3EC6FF'}}>React Native Developer</Text>
          </View>
          <View style={{backgroundColor: '#EFEFEF', paddingHorizontal: 10,paddingVertical:5, marginVertical: 15, borderRadius: 5}}>
            <Text style={{color: '#003366', borderBottomWidth: 1, borderColor: '#003366'}}>Portofolio</Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-around', marginVertical: 10}}>
              <View style={{alignItems: 'center'}}>
                <FontAwesome name="gitlab" size={50} color="#3EC6FF" />
                <Text style={{color: '#003366', marginTop: 5}}>@DwiMunir</Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <AntDesign name="github" size={50} color="#3EC6FF" />
                <Text style={{color: '#003366', marginTop: 5}}>@DwiMunir</Text>
              </View>
            </View>
          </View>
          <View style={{backgroundColor: '#EFEFEF', paddingHorizontal: 10,paddingVertical:5, marginVertical: 15, borderRadius: 5}}>
            <Text style={{color: '#003366', borderBottomWidth: 1, borderColor: '#003366'}}>Hubungi Saya</Text>
            <View style={{paddingHorizontal: 50, marginTop: 15}}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <AntDesign name="facebook-square" size={40} color="#3ec6ff" />
                <Text style={{color: '#003366', marginLeft: 10, fontWeight: 'bold'}}>Dwi Misbachul Munir</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 10}}>
                <AntDesign name="instagram" size={40} color="#3ec6ff" />
                <Text style={{color: '#003366', marginLeft: 10, fontWeight: 'bold'}}>@dwi_m_munir</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}> 
                <AntDesign name="twitter" size={40} color="#3ec6ff" />
                <Text style={{color: '#003366', marginLeft: 10, fontWeight: 'bold'}}>Dwi Misbachul Munir</Text>
              </View>
            </View>
          </View>
        </ScrollView>       
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
  }
})
