import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, ScrollView } from 'react-native'
import logo from '../Images/logo.png'
import { FontAwesome } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class SkillScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems: 'flex-end'}}>
          <Image source={logo} style={styles.logo} />
        </View>
        <View style={{flexDirection: 'row', paddingHorizontal: 10}}>
          <View>
            <FontAwesome name="user-circle" size={36} color="#B4E9FF" />
          </View>
          <View style={{marginLeft: 10}}>
            <Text style={styles.textProfile}>Hai,</Text>
            <Text style={styles.textProfile}>Dwi Misbachul Munir</Text>
          </View>
        </View>

        <View style={{ marginVertical:5}}>
          <Text style={{marginHorizontal: 10,fontSize:24,borderBottomColor: '#B4E9FF', borderBottomWidth:3, marginVertical: 5}}>SKILL</Text>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'center', marginBottom: 5}}>
          <View style={styles.category}>
            <Text style={styles.categoryText}>Framework</Text>
          </View>
          <View style={styles.category}>
            <Text style={styles.categoryText}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.category}>
            <Text style={styles.categoryText}>Teknologi</Text>
          </View>
        </View>
        <ScrollView>
          {this.props.data.items.map((data, key) => (
            <View style={styles.skillWrapp} key={key}>
              <View style={{justifyContent: 'center'}}>
                <MaterialCommunityIcons name={data.iconName} size={64} color="#003366" />
              </View>
              <View>
                <Text style={{fontSize: 24, color: '#003366'}}>{data.skillName}</Text>
                <Text style={{color: '#3ec6ff'}}> {data.categoryName} </Text>
                <View style={{alignItems: 'flex-end'}}>
                  <Text style={{color: 'white', fontSize: 36}}> {data.percentageProgress} </Text>
                </View>
              </View>
              <View style={{justifyContent: 'center', alignItems: 'flex-end'}}>
                <MaterialCommunityIcons name="chevron-right" size={70} color="black" />
              </View>
            </View>
          ))}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logo: {
    resizeMode: 'contain',
    width: 200
  },
  textProfile: {
    fontWeight: '900',
    color: '#003366'
  },
  category: {
    padding: 5,
    backgroundColor: '#B4E9FF',
    marginHorizontal: 5
  },
  categoryText: {
    color: '#003366'
  },
  skillWrapp:{
    padding: 10,
    backgroundColor: '#B4E9FF',
    borderRadius: 5,
    margin: 5,
    marginHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
})
