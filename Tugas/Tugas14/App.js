import React, { Component } from 'react';
import Main from './components/Main';
import SkillScreen from './components/SkillScreen';
import data from './skillData.json'

export default class AppToDo extends Component {
  render() {
    return (
      // <Main />
      <SkillScreen data={data} />
    )
  }
}