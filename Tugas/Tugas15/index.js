import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {SignIn, CreateAccount } from './Screen'
import { TouchableOpacity } from 'react-native';

const AuthStack = createStackNavigator()
const Drawer = createDrawerNavigator()
const Tabs = createBottomTabNavigator();

const HomeScreen =()=>(
  <AuthStack.Navigator>
    <AuthStack.Screen name='SignIn' component={SignIn} options={{title: 'Sign In'}} />
    <AuthStack.Screen name='CreateAccount' component={CreateAccount} options={{title: 'CreateAccount'}} />
    
  </AuthStack.Navigator>
)

const Tab =()=>(
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={HomeScreen} />
    <Tabs.Screen name="Profile" component={HomeScreen} />
    <Tabs.Screen name="Search" component={HomeScreen} />
    <Tabs.Screen name="About" component={HomeScreen} />
  </Tabs.Navigator>
)
export default () => (
  <NavigationContainer>
    <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Notifications" component={Tab} />
    </Drawer.Navigator>
  </NavigationContainer>
)