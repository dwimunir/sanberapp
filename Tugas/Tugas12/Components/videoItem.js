import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icons from 'react-native-vector-icons/MaterialIcons'

export default class VideoItems extends Component{
    render(){
        let vidio = this.props.data
        return(
            <View style={styles.container}>
                <Image source={{uri: vidio[0].snippet.thumbnails.medium.url}} style={{height:200}} />
                <View style={styles.descContainer}>
                    <Image source={{uri: 'https://randomuser.me/api/portraits/men/89.jpg'}} style={{width:50, height:50, borderRadius:25}} />
                    <View style={styles.vidioDetails} >
                        <Text style={styles.vidioTitles}>{vidio[0].snippet.title}</Text>
                        <Text style={styles.channelTitle}> {`${vidio[0].snippet.channelTitle} . ${vidio[0].statistics.viewCount}`} </Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        padding: 15
    },
    descContainer: {
        flexDirection: 'row',
        paddingTop: 15
    },
    vidioDetails: {
        paddingHorizontal: 15,
        flex: 1
    },
    vidioTitles: {
        fontSize: 14,
        color: '#3c3c3c'
    }
})