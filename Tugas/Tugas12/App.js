import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icons from 'react-native-vector-icons/MaterialIcons'
import VideoItems from './Components/videoItem';
import data from './data.json'

export default class App extends Component{
  render(){
    return (
      <View style={styles.container}>
        {/* <StatusBar /> */}
        <View style={styles.navBar}>
          <Image 
            source={require('./images/logo.png')}
            style={{height: 22, width:98}}
          />
          <View style={styles.rightNav}>
            <TouchableOpacity>
              <Icons name='search' size={25} style={styles.navItems} />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icons name='account-circle' size={25} style={styles.navItems} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.body}>
          <VideoItems data={data.items} />
        </View>
        <View style={styles.barNav}>
          <TouchableOpacity style={styles.tabItems}>
            <Icons name='home' size={25} />
            <Text style={styles.tabTittle}>
              Home
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icons name='whatshot' size={25} />
            <Text style={styles.tabTittle}>
              Trending
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icons name='subscriptions' size={25} />
            <Text style={styles.tabTittle}>
              Subscription
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icons name='folder' size={25} />
            <Text style={styles.tabTittle}>
              Library
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  rightNav: {
    flexDirection: "row",
    justifyContent: 'space-around'
  },
  navItems: {
    marginLeft: 25
  },
  body: {
    flex: 1
  },
  barNav: {
    height: 60,
    backgroundColor: 'white',
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  tabItems: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTittle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4
  }
});
