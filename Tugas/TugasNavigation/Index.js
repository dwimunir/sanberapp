import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AboutScreenHome from './AboutScreen';
import LoginScreenHome from './LoginScreen';
import SkillScreen from './SkillScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';
import { Button } from 'react-native';


export default class Index extends Component {
  render() {
    const AuthStack = createStackNavigator()
    return (
      <NavigationContainer>
        <AuthStack.Navigator>
          <AuthStack.Screen name='SignIn' component={LoginScreen} options={{title: 'Sign In'}} />
          <AuthStack.Screen name='AboutScreen' component={AboutScreen} options={{title: 'About Screen'}} />
        </AuthStack.Navigator>
      </NavigationContainer>
      // <NavigationContainer>
      //   <Drawer.Navigator initialRouteName="Home">
      //     <Drawer.Screen name="Login" component={LoginScreen} />
      //     <Drawer.Screen name="About" component={AboutScreen} />
      //   </Drawer.Navigator>
      // </NavigationContainer>
    )
  }
}
const Drawer = createDrawerNavigator()
const LoginScreen = () => (  
  <Drawer.Navigator initialRouteName="Home">
    <Drawer.Screen name="Login" component={LoginScreenHome} />
    <Drawer.Screen name="About" component={AboutScreen} />
  </Drawer.Navigator>
)

const Tabs = createBottomTabNavigator();
const AboutScreen =()=>(
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={SkillScreen} />
    <Tabs.Screen name="Project" component={ProjectScreen} />
    <Tabs.Screen name="Add Screen" component={AddScreen} />
  </Tabs.Navigator>
)

