import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import AppHome from './Tugas/Tugas13/AppHome'
import AppToDo from './Tugas/Tugas14/App';
import AppTutorial from './Tugas/Tugas15'
import Index from './Tugas/TugasNavigation/Index';

export default class App extends Component{
  render(){
    return (
      <View style={styles.container}>
        {/* <StatusBar /> */}
        {/* <AppHome /> */}
        {/* <AppToDo /> */}
        {/* <AppTutorial /> */}
        <Index />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  
});
